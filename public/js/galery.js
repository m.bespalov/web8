/*global
$
*/
window.addEventListener("DOMContentLoaded", function () {
    $(".slider-four-item").slick({
        dots: true,
        slidesToShow: 4,
        speed: 700,
        slidesToScroll: 4,
        infinite: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToScroll: 4,
                    slidesToShow: 4,
                    dots: true
                }
            },
            {
                breakpoint: 740,
                settings: {
                    infinite: true,
                    slidesToScroll: 2,
                    slidesToShow: 2
                }
            }
        ]
    });
});