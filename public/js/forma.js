/*global
    $
    alert
*/

function updateState() {
    let modalButton = document.getElementById("modalButton");
    let storedState = window.localStorage.getItem("modalCheckbox");
    if (storedState === "true") {
        modalButton.disabled = false;
    } else {
        modalButton.disabled = true;
    }
}

function send() {
    let ajaxData = {
        name: document.getElementById("inputModalName").value,
        message: document.getElementById("inputModalMsg").value,
        slap_replyto: document.getElementById("inputModalEmail").value
    };

    $.ajax({
        url: "https://api.slapform.com/dfhfhzjk2393@gmail.com",
        dataType: "json",
        method: "POST",
        data: ajaxData,
        success: function (response) {
            if (response.meta.status === "success") {
                alert("Successful");
            }
            if (response.meta.status === "fail") {
                alert("NOT successful");
            }
        }
    });

    let inputModalName = document.getElementById("inputModalName");
    let inputModalMsg = document.getElementById("inputModalMsg");
    let inputModalEmail = document.getElementById("inputModalEmail");
    let modalCheckbox = document.getElementById("modalCheckbox");

    inputModalName.value = "";
    inputModalMsg.value = "";
    inputModalEmail.value = "";
    modalCheckbox.checked = false;

    window.localStorage.setItem(inputModalName.id, inputModalName.value);
    window.localStorage.setItem(inputModalMsg.id, inputModalMsg.value);
    window.localStorage.setItem(inputModalEmail.id, inputModalEmail.value);
    window.localStorage.setItem(modalCheckbox.id, modalCheckbox.checked);

    updateState();

    return false;
}



window.addEventListener("DOMContentLoaded", function () {

    let inputModalName = document.getElementById("inputModalName");
    let inputModalMsg = document.getElementById("inputModalMsg");
    let modalCheckbox = document.getElementById("modalCheckbox");
    let inputModalEmail = document.getElementById("inputModalEmail");

    inputModalEmail.value = window.localStorage.getItem(inputModalEmail.id);
    inputModalMsg.value = window.localStorage.getItem(inputModalMsg.id);
    inputModalName.value = window.localStorage.getItem(inputModalName.id);

    inputModalName.addEventListener("input", function (event) {
        window.localStorage.setItem(event.target.id, event.target.value);
    });
    inputModalEmail.addEventListener("input", function (event) {
        window.localStorage.setItem(event.target.id, event.target.value);
    });
    inputModalMsg.addEventListener("input", function (event) {
        window.localStorage.setItem(event.target.id, event.target.value);
    });
    modalCheckbox.addEventListener("change", function (event) {
        window.localStorage.setItem(event.target.id, event.target.checked);
        updateState();
    });

    if (window.localStorage.getItem(modalCheckbox.id) === "true") {
        modalCheckbox.checked = true;
    } else {
        modalCheckbox.checked = false;
    }

    $("#formAjax").submit(function () {
        send();
        return false;
    });

    $("#webModal").on("shown.bs.modal", function onShownModal() {
        history.pushState({isShown: true}, "Form shown", "#formShown");
    });
    $("#webModal").on("hidden.bs.modal", function onHiddenModal() {
        history.pushState({isShown: false}, "Form hidden", "");
    });

    if (window.location.hash === "formShown") {
        history.pushState({isShown: true}, "Form shown", "#formShown");
    } else {
        history.pushState({isShown: false}, "Form hidden", "");
    }

    window.onpopstate = function (event) {
        if (event.state.isShown) {
            $("#webModal").modal("show");
        } else {
            $("#webModal").modal("hide");
        }
    };

    updateState();
});