
function getPriceObj() {
    return {
        products: [300, 430, 510],
        options: {
            option1: 400,
            option2: 100,
            option3: 57
        },
        props: {
            prop1: 120,
            prop2: 260
        }
    };
}

function updPrice() {
    let amount = document.getElementsByName("howMuch");
    let sType = document.getElementsByName("consol")[0];
    let CurPrice = 0;
    let prices = getPriceObj();
    let PriceI = parseInt(sType.value) - 1;
    if (PriceI >= 0) {
        CurPrice = prices.products[PriceI];
    }

    let radio = document.getElementById("rad");
    radio.style.display = (
        sType.value === "3"
        ? "block"
        : "none"
    );

    let rad = document.getElementsByName("options");
    rad.forEach(function (radio) {
        if (radio.checked) {
            let OptionalPrice = prices.options[radio.value];
            if (OptionalPrice !== undefined) {
                CurPrice += OptionalPrice;
            }
        }
    });

    let checkDiv = document.getElementById("CheckBoxes");
    checkDiv.style.display = (
        sType.value === "3"
        ? "none"
        : "block"
    );

    let CheckBoxes = document.querySelectorAll("#CheckBoxes input");
    CheckBoxes.forEach(function (checkbox) {
        if (checkbox.checked) {
            let PropertyPrice = prices.props[checkbox.name];
            if (PropertyPrice !== undefined) {
                CurPrice += PropertyPrice;
            }
        }
    });

    document.getElementsByName("howMuch")[0].value = document.getElementsByName("howMuch")[0].value.replace(/[^0-9]/g, "");
    if ((/^[0-9]+$/).test(amount[0].value) && (/^[0-9]+$/).test(amount[0].value)) {
        CurPrice *= amount[0].value;
    }
    let rresult = document.getElementById("rresult");
    rresult.innerHTML = "$ " + CurPrice;
}

window.addEventListener("DOMContentLoaded", function () {
    let radio = document.getElementById("rad");
    radio.style.display = "none";

    let CountedNumber = document.getElementsByName("howMuch")[0];
    CountedNumber.addEventListener("input", function () {
        updPrice();
    });

    let consol = document.getElementsByName("consol")[0];
    consol.addEventListener("change", function () {
        let CheckBoxes = document.querySelectorAll("#CheckBoxes input");
        CheckBoxes.forEach(function (checkbox) {
            checkbox.checked = false;
        });
        let rad = document.getElementsByName("options");
        rad.forEach(function (radio) {
            if (radio.checked) {
                radio.checked = false;
            }
        });
        document.getElementsByName("howMuch")[0].value = 1;
        updPrice();
    });

    document.getElementsByName("options").forEach(function (radio) {
        radio.addEventListener("change", function () {
            updPrice();
        });
    });

    document.querySelectorAll("#CheckBoxes input").forEach(function (checkbox) {
        checkbox.addEventListener("change", function () {
            updPrice();
        });
    });
    updPrice();
});